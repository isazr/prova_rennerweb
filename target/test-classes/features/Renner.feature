#encondig: UTF-8
#author: Isabella Zanatta
@tag
Feature: Automação de Testes Renner

  @web @CT-001
  Scenario: CENARIO BONUS
    When que o usuario clique em Sign in
    Given preenche os dados
    Then o cadastro e realizado com sucesso

  @web @CT-002
  Scenario: Comprar quatro produtos com um cliente ja cadastrado
    When que o usuario cadastrado faca o login
    Given adicionar ao carrinho os produtos "Printed Chiffon Dress" na cor "Green" e tamanho "M"
    And "Faded Short Sleeve T-shirts" na cor "Blue"
    And "Blouse" na quantidade "2"
    And um "Printed Dress"
    Then e validado o produto "Printed Chiffon Dress" adicionado no carrinho
    And e validado o produto "Faded Short Sleeve T-shirts" adicionado no carrinho
    And e validado o produto "Blouse" adicionado no carrinho
    And e validado o produto "Printed Dress" adicionado no carrinho
    And a compra e finalizada
