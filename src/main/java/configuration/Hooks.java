package configuration;

import commons.BaseTest;
import enums.Web;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

public class Hooks extends BaseTest {
	@Before(value = "@web")
	public void beforeScenarioWebChrome() {
		this.inicializeBrowser();
	}

	@After(value = "@web")
	public void afterScenarioWeb(Scenario scenario) throws InterruptedException{
		closeWeb();
	}

	private void inicializeBrowser(){
		try{
			initializeWebApplication(Web.CHROME);
		}catch (IllegalStateException e){
			initializeWebApplication(Web.EDGE);
		}
	}
}
