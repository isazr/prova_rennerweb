package commons;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DSL {
	 private final WebDriver wDriver;
	    private final WebDriverWait wDriverWait;

	    public DSL(WebDriver webDriver){
	        wDriver = webDriver;
	        wDriverWait = new WebDriverWait(wDriver, 5);
	    }

	    public void esperarTitle(String title){
	        wDriverWait.until(ExpectedConditions.titleIs(title));
	    }
	    
	    public void selectCombo(String opcao, WebElement combobox ) throws Exception {
	        Select selecionar = new Select(combobox);
	        if (combobox.isDisplayed()) {
	            selecionar.selectByVisibleText(opcao);
	        } else {
	            throw new Exception("Não foi possível visualizar o elemento" + combobox);

	        }
	    }
	    public boolean aguardarPor(String xpath, int segundos){
	        boolean retorno;
	        try{
	            retorno = new WebDriverWait(wDriver, segundos).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath))).isDisplayed();
	        }catch (Exception e) {
	            retorno = false;
	        }
	        return retorno;
	    }
	    public void selecionarOpcaoComboboxPorPosicao(WebElement elemento, int posicao){
	        Select combobox;
	        wDriverWait.until(ExpectedConditions.elementToBeClickable(elemento));
	        new Actions(wDriver).moveToElement(elemento).perform();
	        combobox = new Select(elemento);
	        if (temOpcoes(combobox)){
	            if (posicao < 0 ){
	                combobox.selectByIndex(combobox.getOptions().size() - 1);
	            }else{
	                combobox.selectByIndex(posicao);
	            }
	        }else throw new IllegalStateException("O combobox não tinha opções.");
	    }
	    
	    private boolean temOpcoes(Select select){
	        int tentativas = 0;
	        boolean retorno = false;
	        do{
	            aguardarPor("//../NãoépraAchar!//" , 5);
	            if (select.getOptions().size() > 0){
	                retorno = true;
	                break;
	            }
	        }while (tentativas < 3);
	        return retorno;
	    }
}
