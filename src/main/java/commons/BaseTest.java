package commons;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import interfaces.WebApplication;

public class BaseTest {
	protected static WebDriver webDriver;
    protected static WebDriverWait wait;
    
    protected void initializeWebApplication(WebApplication webApplication){
        webDriver = webApplication.getDriver();
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        wait = new WebDriverWait(webDriver, 30);
    }
    
    protected static void closeWeb() {
        webDriver.quit();
    }
}
