package enums;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import interfaces.WebApplication;

public enum Web implements WebApplication {
	
	CHROME {
		public WebDriver getDriver() {
			System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
			return new ChromeDriver();
		}
	},
	FIREFOX {
		public WebDriver getDriver() {
			System.setProperty("webdriver.gecko.driver", "geckodriver.exe\"");
			return new ChromeDriver();
		}
	},
	IE {
		public WebDriver getDriver() {
			System.setProperty("webdriver.ie.driver", "iexploredriver.exe");
			return new ChromeDriver();
		}
	},
	EDGE {
		public WebDriver getDriver() {
			System.setProperty("webdriver.edge.driver", "edgedriver.exe");
			return new ChromeDriver();
		}
	},

}
