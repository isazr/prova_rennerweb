package funcionalidades;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import commons.BaseTest;
import commons.DSL;
import pages.AutomationPracticePage;

public class AutomationPracticeFuncionalidade extends BaseTest {
	private final AutomationPracticePage automationPage = new AutomationPracticePage(webDriver);
	private final DSL dsl = new DSL(webDriver);

	public void login() {
		dsl.esperarTitle("My Store");
		automationPage.getSignIn().click();
		dsl.esperarTitle("Login - My Store");
		automationPage.getEmail().sendKeys("aloha@teste.com");
		automationPage.getPassword().sendKeys("12345");
		automationPage.getBtn_submitLogin().click();
		dsl.esperarTitle("My account - My Store");
		String login_success = automationPage.getLogin_success().getText();
		assertEquals("Alora Ferrer", login_success);
	}

	public void procurarProduto(String produto) throws Exception {
		automationPage.getSearch().clear();
		automationPage.getSearch().sendKeys(produto);
		automationPage.getSearch().submit();
		WebElement produto_actual = webDriver
				.findElement(By.xpath("//div[@class='product-image-container']/a[@title='" + produto + "']"));
		produto_actual.click();
		dsl.aguardarPor("//iframe", 3);
		webDriver.switchTo().defaultContent();
		WebElement iframe = webDriver.findElement(By.xpath("//iframe"));
		webDriver.switchTo().frame(iframe);

	}

	public void escolherCor(String cor) {
		WebElement elemento_cor = webDriver.findElement(By.xpath("//a[@name='" + cor + "']"));
		elemento_cor.click();
	}

	public void escolherTamanho(String tamanho) throws Exception {
		WebElement select = webDriver.findElement(By.xpath("//div[@id='uniform-group_1']/select"));
		select.click();
		WebElement tamanhoM = select.findElement(By.xpath("//option[@title='" + tamanho + "']"));
		tamanhoM.click();

	}

	public void escolherQuantidade(String quantidade) {
		automationPage.getInput_qtd().clear();
		automationPage.getInput_qtd().sendKeys(quantidade);
	}

	public void adicionarCarrinho() {
		automationPage.getAdd_carrinho().click();
	}

	public void continuarComprando() {
		wait.until(ExpectedConditions.visibilityOf(automationPage.getContinuar_comprando()));
		automationPage.getContinuar_comprando().click();

	}

	public void finalizarCompra() {
		wait.until(ExpectedConditions.visibilityOf(automationPage.getCheckout()));
		automationPage.getCheckout().click();
	}

	public void validarCarrinho(String produto) {
		webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		WebElement produto_validar = webDriver.findElement(By.xpath("//tr/td/p/a[.='" + produto + "']"));
		
		assertEquals(produto, produto_validar.getText());

	}

	public void checkoutPt2() {
		wait.until(ExpectedConditions.visibilityOf(automationPage.getCheckout_pt2()));
		automationPage.getCheckout_pt2().click();
		wait.until(ExpectedConditions.visibilityOf(automationPage.getCheckout_pt3()));
		automationPage.getCheckout_pt3().click();
		automationPage.getAgreeTerms().click();
		automationPage.getCheckout_pt4().click();
		wait.until(ExpectedConditions.visibilityOf(automationPage.getPayment()));
		automationPage.getPayment().click();
		wait.until(ExpectedConditions.visibilityOf(automationPage.getConfirmOrder()));
		automationPage.getConfirmOrder().click();
		wait.until(ExpectedConditions.visibilityOf(automationPage.getSuccessOrder()));
		String success = automationPage.getSuccessOrder().getText();
		assertEquals("Your order on My Store is complete.", success);
	}
}
