package funcionalidades;


import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;

import commons.BaseTest;
import commons.VariaveisEstaticas;

public class CommonFuncionalidade extends BaseTest {
	VariaveisEstaticas ve = new VariaveisEstaticas();
	
	
	public void acessaSite(String site) {
		webDriver.navigate().to(site);
	}
	
	public String emailAleatorio(){
        Random random = new Random();
        String randomNumber = String.valueOf(random.nextInt(3)+1);
        String randomAlphabetic = RandomStringUtils.randomAlphabetic(10);
        return (randomAlphabetic+randomNumber+"@teste.com"); }
	
	public String randomAlphabetic(int length){
	     return RandomStringUtils.randomAlphabetic(length);
	 }

	    
	    public String randomNumber(int upperBound, int lowerBound){
	        Random random = new Random();
	        return String.valueOf(random.nextInt(upperBound - lowerBound)+ lowerBound);
	    }


}
