package funcionalidades;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import commons.BaseTest;
import commons.DSL;
import pages.AutomationPracticePage;

public class Register extends BaseTest {
	private final AutomationPracticePage automationPage = new AutomationPracticePage(webDriver);
	private final DSL dsl = new DSL(webDriver);
	private final CommonFuncionalidade common = new CommonFuncionalidade();
	String firstName = common.randomAlphabetic(8);
	String lastName = common.randomAlphabetic(8);
	
	public void registerParte1() {
		automationPage.getSignIn().click();
		automationPage.getEmailCreate().sendKeys(common.emailAleatorio());
		automationPage.getSubmitCreate().click();
	}
	
	public void registerParte2() throws Exception {
		

		automationPage.getGender().click();
		automationPage.getFirstName().sendKeys(firstName);
		webDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		automationPage.getLastName().sendKeys(lastName);
		webDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		automationPage.getPassword().sendKeys(common.randomAlphabetic(5));
		webDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		dsl.selecionarOpcaoComboboxPorPosicao(automationPage.getDayRegister(), 10);
		webDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		automationPage.getMonthsRegister().sendKeys("February");
		webDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		automationPage.getYearsRegister().sendKeys("1999");
		webDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		automationPage.getAdressRegister().sendKeys("test st");
		webDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		automationPage.getCityRegister().sendKeys("New York");
		webDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		automationPage.getStateRegister().sendKeys("New York");
		webDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		automationPage.getPostalCodeRegister().sendKeys("10007");
		webDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		automationPage.getCountry().click();
		webDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		automationPage.getPhone().sendKeys("917-331-2800");
		webDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		automationPage.getMyAddress().sendKeys("test st");
		webDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		automationPage.getBtn_register().click();
	}
	
	public void registerParte3() {
		String sucesso = automationPage.getRegister_success().getText();
		equals("My account".equalsIgnoreCase(sucesso));
		String login_sucesso = automationPage.getLogin_success().getText();
		assertEquals(firstName +" "+ lastName, login_sucesso);
	}
}
