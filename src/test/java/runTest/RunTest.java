package runTest;

import org.junit.runner.RunWith;

import commons.BaseTest;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberOptions.SnippetType;


	@RunWith(Cucumber.class)
	@CucumberOptions(monochrome = true,
	        dryRun = false,
	        snippets = SnippetType.CAMELCASE,
	        plugin = {"pretty", "html:target/cucumber-reports" },
	        features = {"./src/test/resources/features"},
	        glue = {"steps", "configuration",
	                "commons", "bean"},

	        tags = "@web")

	public class RunTest extends BaseTest{
}
