package steps;

import funcionalidades.AutomationPracticeFuncionalidade;
import funcionalidades.CommonFuncionalidade;
import funcionalidades.Register;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class AutomationPracticeRenner {
	CommonFuncionalidade common = new CommonFuncionalidade();
	AutomationPracticeFuncionalidade funcionalidade = new AutomationPracticeFuncionalidade();
	Register register = new Register();
	String site = "http://automationpractice.com/";

	@When("que o usuario clique em Sign in")
	public void que_o_usuario_queira_se_cadastrar() {
		common.acessaSite(site);
		register.registerParte1();
	}

	@Given("preenche os dados")
	public void clica_em() throws Exception {
		register.registerParte2();
	}

	@Then("o cadastro e realizado com sucesso")
	public void o_cadastro_e_realizado_com_sucesso() {
		register.registerParte3();
	}

	@When("que o usuario cadastrado faca o login")
	public void que_o_usuario_cadastrado_faca_o_login() {
		common.acessaSite(site);
		funcionalidade.login();
	}

	@Given("adicionar ao carrinho os produtos {string} na cor {string} e tamanho {string}")
	public void adicionar_ao_carrinho_os_produtos_na_cor_e_tamanho(String produto, String cor, String tamanho)
			throws Exception {
		funcionalidade.procurarProduto(produto);
		funcionalidade.escolherCor(cor);
		funcionalidade.escolherTamanho(tamanho);
		funcionalidade.adicionarCarrinho();
		funcionalidade.continuarComprando();

	}

	@Given("{string} na cor {string}")
	public void na_cor(String produto, String cor) throws Exception {
		funcionalidade.procurarProduto(produto);
		funcionalidade.escolherCor(cor);
		funcionalidade.adicionarCarrinho();
		funcionalidade.continuarComprando();
	}

	@Given("{string} na quantidade {string}")
	public void na_quantidade(String produto, String quantidade) throws Exception {
		funcionalidade.procurarProduto(produto);
		funcionalidade.escolherQuantidade(quantidade);
		funcionalidade.adicionarCarrinho();
		funcionalidade.continuarComprando();
	}

	@Given("um {string}")
	public void um(String produto) throws Exception {
		funcionalidade.procurarProduto(produto);
		funcionalidade.adicionarCarrinho();
		funcionalidade.finalizarCompra();
	}

	@Then("e validado o produto {string} adicionado no carrinho")
	public void e_validado_o_produto_na_cor_e_tamanho_adicionado_no_carrinho(String produto) {
		funcionalidade.validarCarrinho(produto);
	}

	@Then("a compra e finalizada")
	public void a_compra_e_finalizada() {
		funcionalidade.checkoutPt2();

	}
}
