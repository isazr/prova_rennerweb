package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AutomationPracticePage {
	
	public AutomationPracticePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//h1")
	private WebElement register_success;
	
	@FindBy(id = "submitAccount")
	private WebElement btn_register;
	
	@FindBy(id = "alias")
	private WebElement myAddress;

	@FindBy(id = "phone_mobile")
	private WebElement phone;
	
	@FindBy(xpath = "//div/select/option[.='United States']")
	private WebElement country;
	
	@FindBy(id = "postcode")
	private WebElement postalcode;
	
	@FindBy(id = "id_state")
	private WebElement state;
	
	@FindBy(id = "city")
	private WebElement city;
	
	@FindBy(id = "address1")
	private WebElement adress;
	
	@FindBy(id = "years")
	private WebElement yearsRegister;
	
	@FindBy(xpath = "//div/select[@id='months']")
	private WebElement monthsRegister;

	@FindBy(id = "days")
	private WebElement dayRegister;
	
	@FindBy(id = "customer_lastname")
	private WebElement lastName;
	
	@FindBy(id = "customer_firstname")
	private WebElement firstName;
	
	@FindBy(id = "id_gender1")
	private WebElement gender;
	
	@FindBy(id = "SubmitCreate")
	private WebElement submitCreate;
	
	@FindBy(id = "email_create")
	private WebElement emailCreate;
	
	@FindBy(xpath = "//a[@class='login']")
	private WebElement signIn;
	
	@FindBy(id = "email")
	private WebElement email;
	
	@FindBy(id = "passwd")
	private WebElement password;
	
	@FindBy(id = "SubmitLogin")
	private WebElement btn_submitLogin;
	
	@FindBy(xpath = "//a[@class='account']")
	private WebElement login_success;
	
	@FindBy(id = "search_query_top")
	private WebElement search;
	
	@FindBy(xpath = "//div[@id='uniform-group_1']/select")
	private WebElement selectTamanho;
	
	@FindBy(xpath = "//a[@class='product-name']")
	private WebElement nome_produto;
	
	@FindBy(xpath = "//button[@name='Submit']")
	private WebElement add_carrinho;
	
	@FindBy(xpath = "//span[@title='Continue shopping']")
	private WebElement continuar_comprando;
	
	@FindBy(xpath = "//input[@name='qty']")
	private WebElement input_qtd;
	
	@FindBy(xpath = "//a[@title='Proceed to checkout']")
	private WebElement checkout;
	
	@FindBy(xpath = "//a/span[.='Proceed to checkout']")
	private WebElement checkout_pt2;
	
	@FindBy(xpath = "//button/span[.='Proceed to checkout']")
	private WebElement checkout_pt3;
	
	@FindBy(xpath = "//button[@name='processCarrier']")
	private WebElement checkout_pt4;
	
	@FindBy(xpath = "//div[@class='checker']/span/input")
	private WebElement agreeTerms;
	
	@FindBy(xpath = "//div/p[@class='payment_module']/a[@class='bankwire']")
	private WebElement payment;

	@FindBy(xpath = "//p/button/span[.='I confirm my order']")
	private WebElement confirmOrder;
	
	@FindBy(xpath = "//div/p/strong")
	private WebElement successOrder;
	
	//GETTERS
	public WebElement getLogin_success() {return login_success;}
	public WebElement getSignIn() {return signIn;}
	public WebElement getEmail() {return email;}
	public WebElement getPassword() {return password;}
	public WebElement getBtn_submitLogin() {return btn_submitLogin;}
	public WebElement getSearch() {return search;}
	public WebElement getSelectTamanho() {return selectTamanho;}
	public WebElement getNome_produto() {return nome_produto;}
	public WebElement getAdd_carrinho() { return add_carrinho;}
	public WebElement getContinuar_comprando() { return continuar_comprando;}
	public WebElement getInput_qtd() {return input_qtd;}
	public WebElement getCheckout() {return checkout;}
	public WebElement getCheckout_pt2() {return checkout_pt2;}
	public WebElement getCheckout_pt3() {return checkout_pt3;}
	public WebElement getCheckout_pt4() {return checkout_pt4;}
	public WebElement getAgreeTerms() {return agreeTerms;}
	public WebElement getPayment() {return payment;}
	public WebElement getConfirmOrder() {return confirmOrder;}
	public WebElement getSuccessOrder() {return successOrder;}
	public WebElement getEmailCreate() {return emailCreate;}
	public WebElement getSubmitCreate() {return submitCreate;}
	public WebElement getGender() {return gender;}
	public WebElement getFirstName() {return firstName;}
	public WebElement getLastName() {return lastName;}
	public WebElement getYearsRegister() {return yearsRegister;}
	public WebElement getMonthsRegister() {return monthsRegister;}
	public WebElement getDayRegister() {return dayRegister;}
	public WebElement getAdressRegister() {return adress;}
	public WebElement getCityRegister() {return city;}
	public WebElement getStateRegister() {return state;}
	public WebElement getPostalCodeRegister() {return postalcode;}
	public WebElement getCountry() {return country;}
	public WebElement getPhone() {return phone;}
	public WebElement getMyAddress() {return myAddress;}
	public WebElement getBtn_register() {return btn_register;}
	public WebElement getRegister_success() {return register_success;}
	
	
	
}
